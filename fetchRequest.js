// Criando as variaveis para a requisicao HTTP
const apiURL = "https://www.mercadobitcoin.net/api/BTC/trades/";

// Fazendo a requisicao pro endpoint usando a lib axios e asynchronous function
async function getBitcoin() {
  getBTCButton.innerHTML = "Loading...";

  try {
    const { data } = await axios.get(apiURL);

    data.forEach(bitcoin => {
      let listBtc = document.getElementById("btc_status");
      let itemBtc = document.createElement("li");
      itemBtc.textContent = `Bitcoin date: ${bitcoin.date}`;
      listBtc.appendChild(itemBtc);
    });

    setTimeout(() => {
      getBTCButton.innerHTML = "Get Bitcoin";
    }, 1000);
    console.log(data);
    console.log(new Date(data[0].date * 1000));
  } catch (e) {
    console.log(e);
  }
}

// Botao que inicializa a requisicao
const getBTCButton = document.getElementById("btc_button");
getBTCButton.addEventListener("click", getBitcoin);
